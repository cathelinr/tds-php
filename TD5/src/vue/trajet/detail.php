<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Détails du trajet</title>
</head>
<body>
<h1>Détails du trajet</h1>
<p><strong>Départ :</strong> <?php echo htmlspecialchars($trajet->getDepart()); ?></p>
<p><strong>Arrivée :</strong> <?php echo htmlspecialchars($trajet->getArrivee()); ?></p>
<p><strong>Date :</strong> <?php echo htmlspecialchars($trajet->getDate()->format('Y-m-d')); ?></p>
<p><strong>Prix :</strong> <?php echo htmlspecialchars($trajet->getPrix()); ?> €</p>
<p><strong>Conducteur :</strong> <?= htmlspecialchars($trajet->getConducteur()->getLogin()) ?></p>
<p><strong>Non Fumeur :</strong> <?= $trajet->isNonFumeur() ? 'Oui' : 'Non' ?></p>

<h2>Passagers :</h2>
<?php if (!empty($trajet->getPassagers())): ?>
    <ul>
        <?php foreach ($trajet->getPassagers() as $passager): ?>
            <li><?php echo htmlspecialchars($passager->getLogin()); ?></li>
        <?php endforeach; ?>
    </ul>
<?php else: ?>
    <p>Pas de passagers pour ce trajet.</p>
<?php endif; ?>
<h2>Inscrire un passager</h2>
<form method="GET" action="controleurFrontal.php">
    <input type="hidden" name="action" value="inscrirePassager">
    <input type="hidden" name="controleur" value="passager">
    <input type="hidden" name="trajetId" value="<?= htmlspecialchars($trajet->getId()) ?>">

    <label for="passagerLogin">Login du passager :</label>
    <input type="text" id="passagerLogin" name="passagerLogin" required>
    <button type="submit">Inscrire</button>
</form>
<br>


<a href="controleurFrontal.php?action=afficherListe&controleur=trajet">Retour à la liste des trajets</a>
</body>
</html>
