<?php
require_once "ConnexionBaseDeDonnees.php";
require_once "Trajet.php";

$trajets = Trajet::recupererTrajets();

foreach ($trajets as $trajet) {
    echo "Trajet de " . $trajet->getDepart() . " à " . $trajet->getArrivee() . " le " . $trajet->getDate()->format('Y-m-d') . "<br>";
    echo "Conducteur : " . $trajet->getConducteur()->getLogin() . "<br>";
    $passagers = $trajet->getPassagers();

    if (!empty($passagers)) {
        echo "Passagers :<br>";
        foreach ($passagers as $passager) {
            echo "- " . $passager->getLogin() . "<br>";
        }
    } else {
        echo "Pas de passagers pour ce trajet.<br>";
    }

    echo "<br>";
}


