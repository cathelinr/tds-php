<?php
namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\Repository\PassagerRepository;
use App\Covoiturage\Modele\Repository\TrajetRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;


class ControleurPassager {

    public static function inscrirePassager(): void {
        if (isset($_GET['passagerLogin'], $_GET['trajetId'])) {
            $loginPassager = $_GET['passagerLogin'];
            $idTrajet = $_GET['trajetId'];

            // Récupération du passager via son login
            $passager = (new UtilisateurRepository())->recupererParClePrimaire($loginPassager);
            if (!$passager) {
                self::afficherErreur("Erreur : Le passager avec le login '$loginPassager' n'existe pas.");
                return;
            }

            // Récupération du trajet via son id
            $trajet = (new TrajetRepository())->recupererParClePrimaire($idTrajet);
            if (!$trajet) {
                self::afficherErreur("Erreur : Le trajet avec l'ID '$idTrajet' n'existe pas.");
                return;
            }

            $success = (new PassagerRepository())->inscrirePassager($trajet, $passager);

            if ($success) {
                // Ajoutez ceci pour vérifier si l'inscription a bien fonctionné
                // Redirection vers la page de détail du trajet après l'inscription réussie
                self::afficherMessage("Passager inscrit avec succès !");
            } else {
                self::afficherErreur("Erreur lors de l'inscription du passager.");
            }
        } else {
            self::afficherErreur("Données manquantes pour l'inscription.");
        }
    }



    public static function desinscrire() : void {
        if (isset($_POST['trajetId'], $_POST['passagerLogin'])) {
            $trajetId = $_POST['trajetId'];
            $passagerLogin = $_POST['passagerLogin'];

            // Désinscription du passager
            (new PassagerRepository())->supprimer($trajetId, $passagerLogin);
            self::afficherMessage("Le passager $passagerLogin a été désinscrit du trajet.");
        } else {
            self::afficherErreur("Données manquantes pour la désinscription.");
        }
    }


    private static function afficherMessage(string $message) : void {
        // Affichage d'un message simple et lien de retour
        echo "<p>$message</p>";
        echo '<p><a href="controleurFrontal.php?action=afficherDetail&controleur=trajet&id=' . $_POST['trajetId'] . '">Retour au trajet</a></p>';
    }

    private static function afficherErreur(string $messageErreur) : void {
        echo "<p>Erreur : " . htmlspecialchars($messageErreur) . "</p>";
        echo '<p><a href="controleurFrontal.php?action=afficherListe&controleur=trajet">Retour à la liste des trajets</a></p>';
    }
}
?>
