<?php
namespace App\Covoiturage\Modele\Repository;
use App\Covoiturage\Modele\DataObject\Trajet;
use App\Covoiturage\Modele\DataObject\Utilisateur;

use PDO;

class PassagerRepository {

    protected function getNomTable(): string {
        return 'passager';
    }

    public function inscrirePassager(Trajet $trajet, Utilisateur $passager): bool {
        $pdo = ConnexionBaseDeDonnees::getPdo();
        $sql = "INSERT INTO passager (trajetId, passagerLogin) VALUES (:trajetId, :passagerLogin)";
        $stmt = $pdo->prepare($sql);
        return $stmt->execute([
            'trajetId' => $trajet->getId(),
            'passagerLogin' => $passager->getLogin()
        ]);
    }

    public function desinscrirePassager(Trajet $trajet, Utilisateur $passager): bool {
        return $this->supprimer([
            'trajetId' => $trajet->getId(),
            'passagerLogin' => $passager->getLogin()
        ]);
    }


    public function supprimer(array $valeursClePrimaire): bool {
        $pdo = ConnexionBaseDeDonnees::getPdo();
        $sql = "DELETE FROM passager WHERE trajetId = :trajetIdTag AND passagerLogin = :passagerLoginTag";
        $stmt = $pdo->prepare($sql);
        return $stmt->execute([
            'trajetIdTag' => $valeursClePrimaire['trajetId'],
            'passagerLoginTag' => $valeursClePrimaire['passagerLogin']
        ]);
    }


    public function recupererPassagersPourTrajet(int $trajetId): array {
        $pdo = ConnexionBaseDeDonnees::getPdo();
        $sql = "SELECT u.* FROM passager p JOIN utilisateur u ON p.passagerLogin = u.login WHERE p.trajetId = :trajetId";
        $stmt = $pdo->prepare($sql);
        $stmt->execute(['trajetId' => $trajetId]);

        $passagers = [];
        while ($utilisateurTableau = $stmt->fetch(PDO::FETCH_ASSOC)) {
            // Utilisation de la nouvelle méthode publique pour construire l'objet Utilisateur
            $passagers[] = (new UtilisateurRepository())->creerUtilisateurDepuisTableau($utilisateurTableau);
        }

        return $passagers;
    }

}
