<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Utilisateur supprimé</title>
</head>
<body>
<p>L'utilisateur de login <?php echo htmlspecialchars($login); ?> a bien été supprimé.</p>

<?php require __DIR__ . '/liste.php';
?>
</body>
</html>
