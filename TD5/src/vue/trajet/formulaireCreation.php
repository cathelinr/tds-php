<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Créer un trajet</title>
</head>
<body>
<h1>Création d'un nouveau trajet</h1>
<form action="controleurFrontal.php?action=creerTrajet&controleur=trajet" method="POST">
    <fieldset>
        <legend>Mon formulaire :</legend>
        <p>
            <label for="depart_id">Départ</label> :
            <input type="text" placeholder="Montpellier" name="depart" id="depart_id" required/>
        </p>
        <p>
            <label for="arrivee_id">Arrivée</label> :
            <input type="text" placeholder="Sète" name="arrivee" id="arrivee_id" required/>
        </p>
        <p>
            <label for="date_id">Date</label> :
            <input type="date" placeholder="JJ/MM/AAAA" name="date" id="date_id" required/>
        </p>
        <p>
            <label for="prix_id">Prix</label> :
            <input type="number" placeholder="20" name="prix" id="prix_id" required/>
        </p>
        <p>
            <label for="conducteurLogin_id">Login du conducteur</label> :
            <input type="text" placeholder="leblancj" name="conducteur" id="conducteurLogin_id" required/>
        </p>
        <p>
            <label for="nonFumeur_id">Non Fumeur ?</label>
            <input type="checkbox" name="nonFumeur" id="nonFumeur_id"/>
        </p>
        <p>
            <input type="submit" value="Créer le trajet"/>
        </p>
    </fieldset>

    <p><a href="controleurFrontal.php?action=afficherListe&controleur=trajet">Retour à la liste des trajets</a></p>
</form>
</body>
</html>
