

<?php
require_once 'Utilisateur.php';

// Récupérer tous les utilisateurs depuis la base de données
$utilisateurs = Utilisateur::recupererUtilisateurs();

foreach ($utilisateurs as $utilisateur) {
    echo "Utilisateur : " . $utilisateur->getLogin() . "<br>";
    echo "Nom : " . $utilisateur->getNom() . "<br>";
    echo "Prénom : " . $utilisateur->getPrenom() . "<br>";

    // Récupération et affichage des trajets en tant que passager
    $trajets = $utilisateur->getTrajetsCommePassager();
    if (!empty($trajets)) {
        echo "Trajets en tant que passager :<br>";
        foreach ($trajets as $trajet) {
            echo "- " . $trajet->getDepart() . " vers " . $trajet->getArrivee() . " le " . $trajet->getDate()->format('Y-m-d') . "<br>";
        }
    } else {
        echo "Aucun trajet en tant que passager.<br>";
    }
    echo "<br>";
}
?>


