<?php
namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\Repository\ConnexionBaseDeDonnees;
use PDO;

abstract class AbstractRepository {
    // Méthode abstraite pour obtenir le nom de la table
    public function recupererParClePrimaire(string $clePrimaire): ?AbstractDataObject {
        $pdo = ConnexionBaseDeDonnees::getPdo();
        $sql = "SELECT * FROM " . $this->getNomTable() . " WHERE " . $this->getNomClePrimaire() . " = :cleTag";
        $stmt = $pdo->prepare($sql);
        $stmt->execute(['cleTag' => $clePrimaire]);

        $objetFormatTableau = $stmt->fetch(\PDO::FETCH_ASSOC);

        if ($objetFormatTableau) {
            return $this->construireDepuisTableauSQL($objetFormatTableau); // Appel dynamique via $this
        }

        return null;
    }
    protected abstract function getNomClePrimaire(): string;

    protected abstract function getNomTable(): string;

    // Méthode abstraite pour construire l'objet à partir d'un tableau SQL
    protected abstract function construireDepuisTableauSQL(array $objetFormatTableau): AbstractDataObject;

    /**
     * Fonction générique pour récupérer tous les objets d'une table
     * @return AbstractDataObject[]
     */
    public function recuperer(): array {
        $pdo = ConnexionBaseDeDonnees::getPdo();
        $sql = "SELECT * FROM " . $this->getNomTable();
        $stmt = $pdo->query($sql);

        $objets = [];
        while ($objetTableau = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $objets[] = $this->construireDepuisTableauSQL($objetTableau);
        }

        return $objets;
    }
    public function supprimer($valeurClePrimaire): bool {
        $pdo = ConnexionBaseDeDonnees::getPdo();
        $sql = "DELETE FROM " . $this->getNomTable() . " WHERE " . $this->getNomClePrimaire() . " = :clePrimaire";
        $stmt = $pdo->prepare($sql);
        return $stmt->execute(['clePrimaire' => $valeurClePrimaire]);
    }
    protected abstract function getNomsColonnes(): array;
    public function ajouter(AbstractDataObject $objet): bool {
        $pdo = ConnexionBaseDeDonnees::getPdo();

        // Récupérer les noms des colonnes
        $nomsColonnes = $this->getNomsColonnes();

        // Construction de la requête SQL dynamique
        $sql = "INSERT INTO " . $this->getNomTable() . " (" . join(", ", $nomsColonnes) . ") VALUES (:" . join("Tag, :", $nomsColonnes) . "Tag)";
        $stmt = $pdo->prepare($sql);

        // Transformation de l'objet en tableau
        $values = $this->formatTableauSQL($objet);

        // Exécution de la requête
        return $stmt->execute($values);
    }
    protected abstract function formatTableauSQL(AbstractDataObject $objet): array;

    public function mettreAJour(AbstractDataObject $objet): void
    {
        $nomTable = $this->getNomTable();
        $nomClePrimaire = $this->getNomClePrimaire();
        $nomsColonnes = $this->getNomsColonnes();

        // Construction de la requête SQL pour la mise à jour
        $champsSet = [];
        foreach ($nomsColonnes as $colonne) {
            $champsSet[] = "$colonne = :{$colonne}Tag";
        }
        $champsSetString = implode(", ", $champsSet);

        $sql = "UPDATE $nomTable SET $champsSetString WHERE $nomClePrimaire = :{$nomClePrimaire}Tag";

        // Debug: afficher la requête générée pour vérifier
        // echo $sql;

        // Récupération du tableau SQL formaté avec les valeurs de l'objet
        $valeurs = $this->formatTableauSQL($objet);

        $pdo = ConnexionBaseDeDonnees::getPdo();
        $stmt = $pdo->prepare($sql);
        $stmt->execute($valeurs);
    }

}
