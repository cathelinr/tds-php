<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Erreur</title>
</head>
<body>
<h1>Erreur</h1>
<p><?php echo htmlspecialchars($messageErreur ?? "Problème avec l'utilisateur."); ?></p>
<a href="controleurFrontal.php?action=afficherListe&controleur=utilisateur">Retour à la liste des utilisateurs</a>
</body>
</html>
