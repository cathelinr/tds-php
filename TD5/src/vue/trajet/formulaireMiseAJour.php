<form method="GET" action="controleurFrontal.php">
    <input type="hidden" name="action" value="mettreAJour">
    <input type="hidden" name="controleur" value="trajet">
    <input type="hidden" name="id" value="<?= htmlspecialchars($trajet->getId()) ?>">

    <label for="depart">Départ :</label>
    <input type="text" name="depart" id="depart" value="<?= htmlspecialchars($trajet->getDepart()) ?>" required><br>

    <label for="arrivee">Arrivée :</label>
    <input type="text" name="arrivee" id="arrivee" value="<?= htmlspecialchars($trajet->getArrivee()) ?>" required><br>

    <label for="date">Date :</label>
    <input type="date" name="date" id="date" value="<?= htmlspecialchars($trajet->getDate()->format('Y-m-d')) ?>" required><br>

    <label for="prix">Prix :</label>
    <input type="number" name="prix" id="prix" value="<?= htmlspecialchars($trajet->getPrix()) ?>" required><br>

    <label for="conducteur">Conducteur :</label>
    <input type="text" name="conducteur" id="conducteur" value="<?= htmlspecialchars($trajet->getConducteur()->getLogin()) ?>" required><br>

    <label for="nonFumeur">Non Fumeur :</label>
    <input type="checkbox" name="nonFumeur" id="nonFumeur" <?= $trajet->isNonFumeur() ? 'checked' : '' ?>><br>

    <button type="submit">Mettre à jour le trajet</button>
</form>
