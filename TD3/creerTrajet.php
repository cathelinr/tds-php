<?php
require_once("Trajet.php");

if (isset($_POST['depart']) && isset($_POST['arrivee']) && isset($_POST['date']) && isset($_POST['conducteur']) && isset($_POST['prix'])) {

    // Récupération des données du formulaire
    $depart = $_POST['depart'];
    $arrivee = $_POST['arrivee'];
    $dateStr = $_POST['date'];
    $conducteurLogin = $_POST['conducteur'];
    $prix = intval($_POST['prix']);
    $nonFumeur = isset($_POST['nonFumeur']) ? 1 : 0;  // Utilisation de isset pour détecter si la case "non-fumeur" est cochée

    // Transformation de la date string en objet DateTime
    $date = DateTime::createFromFormat('Y-m-d', $dateStr);

    // Création de l'objet Utilisateur pour le conducteur
    $conducteur = Utilisateur::recupererUtilisateurParLogin($conducteurLogin);

    // Création de l'objet Trajet avec les données récupérées
    $trajet = new Trajet(null, $depart, $arrivee, $date, $prix, $conducteur, $nonFumeur);

    // Appel de la méthode pour insérer le trajet dans la base de données
    $trajet->ajouter();

    // Afficher le trajet créé
    echo $trajet;
} else {
    echo "Tous les champs obligatoires n'ont pas été remplis.";
}
?>
