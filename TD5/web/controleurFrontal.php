<?php

// Inclure l'autoloader PSR-4 pour charger automatiquement les classes
require_once __DIR__ . '/../src/Lib/Psr4AutoloaderClass.php';

// Initialisation du chargeur de classes
$chargeurDeClasse = new App\Covoiturage\Lib\Psr4AutoloaderClass(false);
$chargeurDeClasse->register();
$chargeurDeClasse->addNamespace('App\Covoiturage', __DIR__ . '/../src');

// Si aucun paramètre d'action n'est passé dans l'URL, utiliser 'afficherListe' comme action par défaut
$action = isset($_GET['action']) ? $_GET['action'] : 'afficherListe';

// Récupération du contrôleur dans l'URL (utilisateur ou trajet)
$controleur = isset($_GET['controleur']) ? $_GET['controleur'] : 'utilisateur';

$nomDeClasseControleur = 'App\\Covoiturage\\Controleur\\Controleur' . ucfirst($controleur);
if (class_exists($nomDeClasseControleur)) {
    if (method_exists($nomDeClasseControleur, $action)) {
        call_user_func([$nomDeClasseControleur, $action]);
    } else {
        App\Covoiturage\Controleur\ControleurUtilisateur::afficherErreur("L'action $action n'existe pas.");
    }
} else {
    App\Covoiturage\Controleur\ControleurUtilisateur::afficherErreur("Le contrôleur $controleur n'existe pas.");
}

