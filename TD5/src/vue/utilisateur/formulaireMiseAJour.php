<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Mettre à jour l'utilisateur</title>
</head>
<body>
<h1>Mettre à jour l'utilisateur : <?= htmlspecialchars($utilisateur->getLogin()) ?></h1>
<form method="get" action="controleurFrontal.php">
    <input type="hidden" name="action" value="mettreAJour">
    <input type="hidden" name="controleur" value="utilisateur">

    <fieldset>
        <legend>Formulaire de mise à jour :</legend>

        <p>
            <label for="login_id">Login</label> :
            <input type="text" name="login" id="login_id" value="<?= htmlspecialchars($utilisateur->getLogin()) ?>" readonly />
        </p>

        <p>
            <label for="prenom_id">Prenom</label> :
            <input type="text" name="prenom" id="prenom_id" value="<?= htmlspecialchars($utilisateur->getPrenom()) ?>" required />
        </p>

        <p>
            <label for="nom_id">Nom</label> :
            <input type="text" name="nom" id="nom_id" value="<?= htmlspecialchars($utilisateur->getNom()) ?>" required />
        </p>

        <p>
            <input type="submit" value="Mettre à jour">
        </p>
    </fieldset>
</form>
</body>
</html>
