<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title> Mon premier php </title>
    </head>
   
    <body>
        Voici le résultat du script PHP : 
        <?php
          // Ceci est un commentaire PHP sur une ligne
          /* Ceci est le 2ème type de commentaire PHP
          sur plusieurs lignes */
           
          // On met la chaine de caractères "hello" dans la variable 'texte'
          // Les noms de variable commencent par $ en PHP
          $texte = "hello world !";

          // On écrit le contenu de la variable 'texte' dans la page Web
        $nom = "AAA";
        $prenom = "BBB";
        $login = "CCC";
        $utilisateur = [
                'nom' => 'AAA',
            'prenom' => 'BBB',
            'login' => 'CCC'
        ];
        $utilisateur2 = [
            'nom' => 'AAAa',
            'prenom' => 'BBBb',
            'login' => 'CCCc'
        ];
        $utilisateurs = [$utilisateur, $utilisateur2];
        echo "<h2>Liste des Utilisateurs</h2>";
        echo "<ul>";
        if (sizeof($utilisateurs) == 0){
            echo "Il n'y a aucun utilisateur";
        }
        foreach ($utilisateurs as $cle => $valeur){
            echo "<li>.$valeur[nom] $valeur[prenom] $valeur[login].</li>";
        }
        echo "</ul>";



        var_dump($utilisateurs);
        //var_dump($utilisateur);
        //echo "Utilisateur $utilisateur[nom] $utilisateur[prenom] de login $utilisateur[login]";

        //echo "Utilisateur $prenom $nom de login $login";

          // echo $texte;
        ?>
    </body>
</html> 