<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Erreur utilisateur</title>
</head>
<body>
<h1>Problème avec l'utilisateur</h1>
<p>Impossible de trouver l'utilisateur demandé ou aucun login n'a été fourni.</p>
<p><a href="routeur.php?action=afficherListe">Retour à la liste des utilisateurs</a></p>
</body>
</html>
