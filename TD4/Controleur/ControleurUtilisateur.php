<?php
require_once ('../Modele/ModeleUtilisateur.php');
class ControleurUtilisateur {
    public static function afficherListe() : void {
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurs();
        self::afficherVue('utilisateur/liste.php', ['utilisateurs' => $utilisateurs]);
    }


    public static function afficherDetail() : void {
        if (isset($_GET['login'])) {
            $login = $_GET['login'];
            $utilisateur = ModeleUtilisateur::recupererUtilisateurParLogin($login);

            if ($utilisateur) {
                // On passe l'utilisateur récupéré à la vue detail.php
                self::afficherVue('utilisateur/detail.php', ['utilisateurEnParametre' => $utilisateur]);
            } else {
                // Si l'utilisateur n'est pas trouvé
                self::afficherVue('utilisateur/erreur.php');
            }
        } else {
            // Si aucun login n'est passé
            self::afficherVue('utilisateur/erreur.php');
        }
    }

    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        // Crée des variables à partir du tableau $parametres
        extract($parametres);
        require "../vue/$cheminVue";
    }
    public static function afficherFormulaireCreation() {
        // Appel à la méthode afficherVue pour afficher le formulaire
        self::afficherVue('utilisateur/formulaireCreation.php');
    }
    public static function creerDepuisFormulaire() {
        if (isset($_GET['login']) && isset($_GET['nom']) && isset($_GET['prenom'])) {
            $login = $_GET['login'];
            $nom = $_GET['nom'];
            $prenom = $_GET['prenom'];

            // Création d'une instance de l'utilisateur
            $nouvelUtilisateur = new Utilisateur($login, $nom, $prenom);

            // Appel de la méthode pour ajouter l'utilisateur à la base de données
            ModeleUtilisateur::ajouterUtilisateur($nouvelUtilisateur);
            self::afficherListe();
        } else {
            self::afficherVue('utilisateur/erreur.php');
        }
    }
}
?>

