<?php

require_once 'ConnexionBaseDeDonnees.php';
require_once 'Utilisateur.php';

class Trajet {

    private ?int $id;
    private string $depart;
    private string $arrivee;
    private DateTime $date;
    private int $prix;
    private Utilisateur $conducteur;
    private bool $nonFumeur;
    private array $passagers;
    public function __construct(
        ?int $id,
        string $depart,
        string $arrivee,
        DateTime $date,
        int $prix,
        Utilisateur $conducteur,
        bool $nonFumeur,
        array $passagers = []
    )
    {
        $this->id = $id;
        $this->depart = $depart;
        $this->arrivee = $arrivee;
        $this->date = $date;
        $this->prix = $prix;
        $this->conducteur = $conducteur;
        $this->nonFumeur = $nonFumeur;
        $this->passagers = $passagers;
    }

    public static function construireDepuisTableauSQLllllll(array $trajetTableau) : Trajet {
        return new Trajet(
            $trajetTableau["id"],
            $trajetTableau["depart"],
            $trajetTableau["arrivee"],
            $trajetTableau["date"],
            $trajetTableau["prix"],
            Utilisateur::recupererUtilisateurParLogin($trajetTableau['conducteurLogin']),            $trajetTableau["nonFumeur"],
            $trajetTableau->setPassagers($trajetTableau->recupererPassagers())
        );
    }

    public static function construireDepuisTableauSQL(array $trajetTableau): Trajet {
        $conducteur = Utilisateur::recupererUtilisateurParLogin($trajetTableau['conducteurLogin']);
        $trajet = new Trajet(
            $trajetTableau["id"],
            $trajetTableau["depart"],
            $trajetTableau["arrivee"],
            new DateTime($trajetTableau["date"]),
            $trajetTableau["prix"],
            $conducteur,
            $trajetTableau["nonFumeur"],
        );
        $trajet->setPassagers($trajet->recupererPassagers());

        return $trajet;
    }




    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getDepart(): string
    {
        return $this->depart;
    }

    public function setDepart(string $depart): void
    {
        $this->depart = $depart;
    }

    public function getArrivee(): string
    {
        return $this->arrivee;
    }

    public function setArrivee(string $arrivee): void
    {
        $this->arrivee = $arrivee;
    }

    public function getDate(): DateTime
    {
        return $this->date;
    }

    public function setDate(DateTime $date): void
    {
        $this->date = $date;
    }

    public function getPrix(): int
    {
        return $this->prix;
    }

    public function setPrix(int $prix): void
    {
        $this->prix = $prix;
    }

    public function getConducteur(): Utilisateur
    {
        return $this->conducteur;
    }

    public function setConducteur(Utilisateur $conducteur): void
    {
        $this->conducteur = $conducteur;
    }

    public function isNonFumeur(): bool
    {
        return $this->nonFumeur;
    }

    public function setNonFumeur(bool $nonFumeur): void
    {
        $this->nonFumeur = $nonFumeur;
    }
    public function getPassagers(): array {
        return $this->passagers;
    }

    public function setPassagers(array $passagers): void {
        $this->passagers = $passagers;
    }

    /**
     * @return Trajet[]
     */
    public static function recupererTrajets() : array {
        $pdoStatement = ConnexionBaseDeDonnees::getPDO()->query("SELECT * FROM trajet");
        $trajets = [];
        foreach($pdoStatement as $trajetFormatTableau) {
            $trajets[] = Trajet::construireDepuisTableauSQL($trajetFormatTableau);
        }

        return $trajets;
    }
    public function ajouter()
    {
        try {
            $pdo = ConnexionBaseDeDonnees::getPdo();

            $sql = "INSERT INTO trajet (depart, arrivee, date, prix, conducteurLogin, nonFumeur)
                    VALUES (:depart, :arrivee, :date, :prix, :conducteurLogin, :nonFumeur)";

            $stmt = $pdo->prepare($sql);
            $values = array(
                'depart' => $this->depart,
                'arrivee' => $this->arrivee,
                'date' => $this->date->format('Y-m-d'),  // Conversion DateTime en string
                'prix' => $this->prix,
                'conducteurLogin' => $this->conducteur->getLogin(),
                'nonFumeur' => $this->nonFumeur ? 1 : 0  // Conversion booléen en entier (1 ou 0)
            ); //zzz Zinedine Zidane
            // Exécution de la requête préparée avec les valeurs
            $stmt->execute($values);
            echo "Trajet ajouté avec succès.";
        } catch (PDOException $e) {
            // En cas d'erreur, afficher le message d'erreur
            echo "Erreur lors de l'ajout du trajet : " . $e->getMessage();
        }
    }

    /**
     * @return Utilisateur[]
     */
    private function recupererPassagers() : array {
        $pdo = ConnexionBaseDeDonnees::getPdo();
        $sql = "SELECT u.* 
            FROM passager p
            JOIN utilisateur u ON p.passagerLogin = u.login
            WHERE p.trajetId = :trajetId";
        $stmt = $pdo->prepare($sql);
        $stmt->execute(['trajetId' => $this->id]);
        $passagers = [];

        while ($utilisateurTableau = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $passagers[] = Utilisateur::construireDepuisTableauSQL($utilisateurTableau);
        }
        return $passagers;
    }

    public function __toString() : string {
        return "Trajet de {$this->depart} à {$this->arrivee} le {$this->date->format('Y-m-d')}, 
                conducteur : {$this->conducteur->getLogin()}, 
                " . ($this->nonFumeur ? "Non-fumeur" : "Fumeur autorisé");
    }
}