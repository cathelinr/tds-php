<h1>Trajet mis à jour avec succès</h1>
<p>Le trajet entre <?= htmlspecialchars($trajet->getDepart()) ?> et <?= htmlspecialchars($trajet->getArrivee()) ?> a bien été mis à jour.</p>
<a href="controleurFrontal.php?action=afficherListe&controleur=trajet">Retour à la liste des trajets</a>
