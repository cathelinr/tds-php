<?php
require_once 'ControleurUtilisateur.php';

// On récupère l'action passée dans l'URL via le query string
if (isset($_GET['action'])) {
    $action = $_GET['action'];

    // Appel des actions
    if ($action === 'afficherFormulaireCreation') {
        ControleurUtilisateur::afficherFormulaireCreation();
    } elseif ($action === 'creerDepuisFormulaire') {
        ControleurUtilisateur::creerDepuisFormulaire();
    } elseif ($action === 'afficherListe') {
        ControleurUtilisateur::afficherListe();
    } elseif ($action === 'afficherDetail') {
        ControleurUtilisateur::afficherDetail();
    }}
?>
