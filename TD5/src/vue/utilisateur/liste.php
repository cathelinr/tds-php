<html>
<body>
<u><h1>Liste des Utilisateurs:</h1></u>
<style>
    li {
        margin-bottom: 10px; /* Ajoute un peu d'espace entre les éléments de la liste */
    }
    .supprimer {
        margin-left: 10px; /* Espace entre le nom de l'utilisateur et le lien "Supprimer" */
        color: red; /* Change la couleur du lien "Supprimer" en rouge */
    }
    .mettreAJour {
        margin-left: 10px;
    }
</style>
<ul>
    <?php
    /** @var ModeleUtilisateur[] $utilisateurs */

    foreach ($utilisateurs as $utilisateur): ?>
        <li>
            <?php echo htmlspecialchars($utilisateur->getLogin()); ?>
            <a href="controleurFrontal.php?action=afficherDetail&controleur=utilisateur&login=<?= urlencode($utilisateur->getLogin()); ?>">Détails</a>
            <a class="mettreAJour" href="controleurFrontal.php?action=afficherFormulaireMiseAJour&controleur=utilisateur&login=<?= urlencode($utilisateur->getLogin()); ?>">Mettre à jour</a>
            <a class="supprimer" href="controleurFrontal.php?action=supprimer&controleur=utilisateur&login=<?php echo urlencode($utilisateur->getLogin()); ?>"
               onclick="return confirm('Êtes-vous sûr de vouloir supprimer cet utilisateur ?');">Supprimer</a>
        </li>
    <?php endforeach; ?>
</ul>
<p><a href="controleurFrontal.php?action=afficherFormulaireCreation&controleur=utilisateur">Créer un utilisateur</a></p>
</body>
</html>
