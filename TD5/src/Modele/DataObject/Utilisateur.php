<?php
namespace App\Covoiturage\Modele\DataObject;
use App\Covoiturage\Modele\Repository\ConnexionBaseDeDonnees;
class Utilisateur extends AbstractDataObject
{
    private string $login;
    private string $nom;
    private string $prenom;
    private ?array $trajetsCommePassager = null;

    // un getter
    public function getNom() : string {
        return $this->nom;
    }

    // un setter
    public function setNom(string $nom) {
        $this->nom = $nom;
    }

    /**
     * @return mixed
     */
    public function getLogin() : string
    {
        return $this->login;
    }

    /**
     * @param mixed $login
     */
    public function setLogin(string $login)
    {
        $this->login = substr($login,0,64);
    }

    /**
     * @return mixed
     */
    public function getPrenom() : string
    {
        return $this->prenom;
    }

    /**
     * @param mixed $prenom
     */
    public function setPrenom(string $prenom)
    {
        $this->prenom = $prenom;
    }

    // un constructeur
    public function __construct(
        string $login,
        string $nom,
        string $prenom
   ) {
        $this->login = substr($login,0,64);
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->trajetsCommePassager = null;
    }



    // Pour pouvoir convertir un objet en chaîne de caractères
    /*public function __toString() : string{
        return" $this->login  $this->prenom  $this->nom";
        // À compléter dans le prochain exercice
    }
    */

    /*

    public static function construireDepuisTableauSQL(array $utilisateurFormatTableau) : Utilisateur {
        return new Utilisateur(
            $utilisateurFormatTableau['login'],
            $utilisateurFormatTableau['nom'],
            $utilisateurFormatTableau['prenom']
        );
    }

    */


    /*

    public static function recupererUtilisateurParLogin(string $login) : ?Utilisateur {
        $sql = "SELECT * from utilisateur WHERE login = :loginTag";
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "loginTag" => $login,
            //nomdutag => valeur, ...
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);

        // On récupère les résultats comme précédemment
        // Note: fetch() renvoie false si pas d'utilisateur correspondant
        $utilisateurFormatTableau = $pdoStatement->fetch();
        if (!$utilisateurFormatTableau){
            return null;
        }


        return Utilisateur::construireDepuisTableauSQL($utilisateurFormatTableau);
    }

    */


    /*

    public function ajouter(): void {
        $sql = "INSERT INTO utilisateur(login, nom, prenom) VALUES(:login, :nom, :prenom)";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = array(
            "login" => $this->login,
            "nom" => $this->nom,
            "prenom" => $this->prenom,
        );
        $pdoStatement->execute($values);
    }

    */

    private function recupererTrajetsCommePassager(): array {
        // Si les trajets sont déjà chargés, on les retourne
        if ($this->trajetsCommePassager !== null) {
            return $this->trajetsCommePassager;
        }

        // Récupération des trajets depuis la base de données
        $pdo = ConnexionBaseDeDonnees::getPdo();
        $sql = "SELECT t.* FROM trajet t
            JOIN passager p ON t.id = p.trajetId
            WHERE p.passagerLogin = :login";

        // Exécution de la requête préparée
        $stmt = $pdo->prepare($sql);
        $stmt->execute(['login' => $this->login]);

        $this->trajetsCommePassager = [];

        // Pour chaque ligne de résultat, on construit un objet Trajet
        while ($trajetTableau = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $this->trajetsCommePassager[] = Trajet::construireDepuisTableauSQL($trajetTableau);
        }

        // Retourner les trajets
        return $this->trajetsCommePassager;
    }

    public function getTrajetsCommePassager(): array {
        // Chargement paresseux des trajets
        if ($this->trajetsCommePassager === null) {
            $this->trajetsCommePassager = $this->recupererTrajetsCommePassager();
        }
        return $this->trajetsCommePassager;
    }

    public function setTrajetsCommePassager(?array $trajetsCommePassager): void
    {
        $this->trajetsCommePassager = $trajetsCommePassager;
    }

    /*
    public static function recupererUtilisateurs(): array {
        $pdo = ConnexionBaseDeDonnees::getPdo();
        $sql = "SELECT * FROM utilisateur";
        $stmt = $pdo->query($sql);

        $utilisateurs = [];
        while ($utilisateurTableau = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $utilisateurs[] = Utilisateur::construireDepuisTableauSQL($utilisateurTableau);
        }

        return $utilisateurs;
    }
    */





    /* inutile
    public static function ajouterUtilisateur(Utilisateur $utilisateur) {
        $sql = "INSERT INTO utilisateur (login, nom, prenom) VALUES (:login, :nom, :prenom)";
        $pdo = ConnexionBaseDeDonnees::getPdo();
        $stmt = $pdo->prepare($sql);
        $stmt->execute([
            'login' => $utilisateur->getLogin(),
            'nom' => $utilisateur->getNom(),
            'prenom' => $utilisateur->getPrenom()
        ]);
    }
    */



}