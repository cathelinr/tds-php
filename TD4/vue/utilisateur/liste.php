<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Liste des utilisateurs</title>
</head>
<body>


<?php
/** @var ModeleUtilisateur[] $utilisateurs */
foreach ($utilisateurs as $utilisateur): ?>
    <li>
        <a href="routeur.php?action=afficherDetail&login=<?php echo urlencode($utilisateur->getLogin()); ?>">
            <?php echo $utilisateur->getLogin(); ?></a></li>
<?php endforeach; ?>
<p><a href="routeur.php?action=afficherFormulaireCreation">Créer un utilisateur</a></p>
</body>
</html>
