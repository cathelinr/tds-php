<?php
namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\Repository\TrajetRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use App\Covoiturage\Modele\DataObject\Trajet;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use \DateTime;
class ControleurTrajet {

    public static function afficherListe() : void {
        // Utilisation de la méthode recuperer() via une instance de TrajetRepository
        $trajets = (new TrajetRepository())->recuperer();
        self::afficherVueGenerale('trajet/liste.php', [
            'trajets' => $trajets,
            'titre' => 'Liste des trajets',
            'cheminCorpsVue' => 'trajet/liste.php'
        ]);
    }
    private static function afficherVueGenerale(string $cheminCorpsVue, array $parametres = []) : void {
        extract($parametres);
        require __DIR__ . '/../vue/vueGenerale.php';
    }

    public static function afficherErreur(string $messageErreur = "") : void {
        $messageErreur = $messageErreur ? "Problème avec le trajet : " . htmlspecialchars($messageErreur) : "Problème avec le trajet.";
        self::afficherVueGenerale('trajet/erreur.php', ['messageErreur' => $messageErreur, 'titre' => 'Erreur']);
    }

    public static function creerTrajet() : void {
        if (isset($_POST['depart'], $_POST['arrivee'], $_POST['date'], $_POST['prix'], $_POST['conducteur'])) {
            // Récupération des données du formulaire
            $depart = $_POST['depart'];
            $arrivee = $_POST['arrivee'];
            $dateStr = $_POST['date'];
            $conducteurLogin = $_POST['conducteur']; // Login du conducteur récupéré dans le champ texte
            $prix = intval($_POST['prix']);
            $nonFumeur = isset($_POST['nonFumeur']) ? 1 : 0; // Vérification de la case "Non Fumeur"

            // Transformation de la date string en objet DateTime
            $date = DateTime::createFromFormat('Y-m-d', $dateStr);

            // Récupération du conducteur par son login
            $conducteur = (new UtilisateurRepository())->recupererParClePrimaire($conducteurLogin);

            // Vérification que le conducteur existe bien dans la base de données
            if (!$conducteur) {
                echo "Erreur : Conducteur introuvable.";
                self::afficherFormulaireCreation();
                return;
            }

            // Création de l'objet Trajet avec les données récupérées
            $nouveauTrajet = new Trajet(null, $depart, $arrivee, $date, $prix, $conducteur, $nonFumeur);

            // Appel de la méthode pour insérer le trajet dans la base de données
            (new TrajetRepository())->ajouter($nouveauTrajet);
            // Afficher la liste des trajets après la création
            self::afficherListe();
        } else {
            // Si les données ne sont pas toutes remplies, réafficher le formulaire
            self::afficherFormulaireCreation();
        }
    }
    public static function supprimer(): void
    {
        if (isset($_GET['id'])) {
            $id = $_GET['id'];

            (new TrajetRepository())->supprimer($id);  // Appel à la méthode générique supprimer
            $trajets = (new TrajetRepository())->recuperer();
            self::afficherVueGenerale('trajet/trajetSupprime.php', [
                'id' => $id,
                'trajets' => $trajets
            ]);
        } else {
            self::afficherErreur("ID de trajet manquant pour la suppression.");
        }
    }




    public static function afficherDetail() : void {
        if (isset($_GET['id'])) {
            $id = $_GET['id'];
            $trajet = (new TrajetRepository())->recupererParClePrimaire($id);  // Utilisation de recupererParClePrimaire

            if ($trajet) {
                self::afficherVueGenerale('trajet/detail.php', [
                    'trajet' => $trajet,
                    'titre' => 'Détail du trajet',
                    'cheminCorpsVue' => 'trajet/detail.php'
                ]);
            } else {
                self::afficherErreur("Trajet avec l'ID '" . htmlspecialchars($id) . "' introuvable.");
            }
        } else {
            self::afficherErreur("Aucun ID spécifié.");
        }
    }

    public static function afficherFormulaireCreation() : void {
        self::afficherVueGenerale('trajet/formulaireCreation.php', [
            'titre' => 'Créer un trajet',
            'cheminCorpsVue' => 'trajet/formulaireCreation.php'
        ]);
    }


    private static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Trajet {
        if (empty($tableauDonneesFormulaire['depart'])) {
            throw new \Exception("Le champ 'Départ' est requis.");
        }
        if (empty($tableauDonneesFormulaire['arrivee'])) {
            throw new \Exception("Le champ 'Arrivée' est requis.");
        }
        if (empty($tableauDonneesFormulaire['date'])) {
            throw new \Exception("Le champ 'Date' est requis.");
        }
        if (empty($tableauDonneesFormulaire['prix']) || !is_numeric($tableauDonneesFormulaire['prix'])) {
            throw new \Exception("Le champ 'Prix' est requis et doit être un nombre.");
        }
        if (empty($tableauDonneesFormulaire['conducteur'])) {
            throw new \Exception("Le champ 'Conducteur' est requis.");
        }

        $depart = $tableauDonneesFormulaire['depart'];
        $arrivee = $tableauDonneesFormulaire['arrivee'];
        $date = new DateTime($tableauDonneesFormulaire['date']);
        $prix = intval($tableauDonneesFormulaire['prix']);
        $nonFumeur = isset($tableauDonneesFormulaire['nonFumeur']) ? 1 : 0;

        $conducteurLogin = $tableauDonneesFormulaire['conducteur'];
        $conducteur = (new UtilisateurRepository())->recupererParClePrimaire($conducteurLogin);

        if (!$conducteur) {
            throw new \Exception("Erreur : Conducteur introuvable.");
        }

        // Création du nouvel objet Trajet avec un id potentiellement null
        $id = $tableauDonneesFormulaire['id'] ?? null;
        return new Trajet($id, $depart, $arrivee, $date, $prix, $conducteur, $nonFumeur);
    }


    public static function afficherFormulaireMiseAJour() : void {
        if (isset($_GET['id'])) {
            $id = $_GET['id'];
            $trajet = (new TrajetRepository())->recupererParClePrimaire($id);

            if ($trajet) {
                self::afficherVueGenerale('trajet/formulaireMiseAJour.php', [
                    'trajet' => $trajet,
                    'titre' => 'Mise à jour du trajet'
                ]);
            } else {
                self::afficherErreur("Trajet non trouvé.");
            }
        } else {
            self::afficherErreur("Aucun ID de trajet spécifié.");
        }
    }

    public static function mettreAJour(): void {
        if (isset($_GET['id'], $_GET['depart'], $_GET['arrivee'], $_GET['date'], $_GET['prix'], $_GET['conducteur'])) {
            try {
                // Utilisation de la méthode construireDepuisFormulaire avec $_GET
                $nouveauTrajet = self::construireDepuisFormulaire($_GET);

                // Appel de la méthode mettreAJour du repository
                (new TrajetRepository())->mettreAJour($nouveauTrajet);

                // Affichage de la confirmation de mise à jour
                self::afficherVueGenerale('trajet/trajetMisAJour.php', [
                    'trajet' => $nouveauTrajet,
                    'titre' => 'Trajet mis à jour'
                ]);
            } catch (\Exception $e) {
                self::afficherErreur($e->getMessage());
            }
        } else {
            self::afficherErreur("Données manquantes pour la mise à jour.");
        }
    }







    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres);
        require __DIR__ . '/../vue/' . $cheminVue;
    }
}
?>